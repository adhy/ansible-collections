Collections of ansible configs with the following use-cases:
- ansible-playbook is run through bastion host to the target servers
- all target servers are configured to accept public_key from ansible host (in this case, my macbook pro)